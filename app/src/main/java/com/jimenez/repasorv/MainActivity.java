package com.jimenez.repasorv;


import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    //paso10
    RecyclerView rvElementos;
    //paso13.1
    Adaptador miAdaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //paso11
        rvElementos=findViewById(R.id.rvElementos);
        //paso14
        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //Este paso es para una lista ----------------------------------------------------------
        //paso12 lista de datos locales
        List<String> miDatosLocales = new ArrayList<>();
        miDatosLocales.add("Paulo");
        miDatosLocales.add("Cortes");
        miDatosLocales.add("Bogotá");
        //---------------------------------------------------------------------------------------

        //paso15.2 utilizar room para la BD
        List<String> misDatoString=Arrays.asList(getResources().getStringArray(R.array.nombres));

        //Paso13
        //Local------------------------------------------------------------------------------
        //miAdaptador = new Adaptador(miDatosLocales);
        //-----------------------------------------------------------------------------------
        miAdaptador = new Adaptador(misDatoString);
        rvElementos.setAdapter(miAdaptador);
        //----------------------------------------------------------------------------------
        //Paso17.2
        getElementos();

    }

    //Paso 16Llamar elementos de una BD de una URL
    public void getElementos(){

        String miurl= "https://www.balldontlie.io/api/v1/teams"; //retorna Json
        RequestQueue miPila= Volley.newRequestQueue(this);

        //Realizar peticion para cargarla en el RecyclerView
        JsonObjectRequest miPeticion;

        //Paso 17 Inicializar una peticion valida
        miPeticion = new JsonObjectRequest(Request.Method.GET, miurl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //obtener datos de la API
                        try {
                            //cargar string full name de cada uno de los equipos
                            JSONArray miArreglo=response.getJSONArray("data");
                            List<String> elementos=new ArrayList<>();
                            for (int i=0; i<miArreglo.length();i++){
                                JSONObject miElemento=miArreglo.getJSONObject(i);
                                elementos.add(miElemento.getString("full_name"));
                            }

                            //mostrar los resultados en el recyclerView
                            RecyclerView rv=findViewById(R.id.rvElementos);
                            miAdaptador=new Adaptador(elementos);
                            rv.setAdapter(miAdaptador);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG,"Hay un error");
                    }
        }
        );//Evento asincrono
        miPila.add(miPeticion);
    }
}