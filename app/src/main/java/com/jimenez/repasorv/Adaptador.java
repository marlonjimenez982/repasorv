package com.jimenez.repasorv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adaptador extends RecyclerView.Adapter <Elemento_caja>{ //paso4

    List<String> lElementos; //paso5 lista de elementos

    public Adaptador(List<String>datos){ //paso6 Constructor
        lElementos=datos;
    }

    @NonNull
    @Override
    public Elemento_caja onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //paso 7
        View miCajon;
        miCajon = LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento_caja,parent,false);
        return new Elemento_caja(miCajon);

    }

    @Override
    public void onBindViewHolder(@NonNull Elemento_caja holder, int position) {

        //paso 8
        String miDato = lElementos.get(position);
        holder.txtElemento.setText(miDato);

    }

    @Override
    public int getItemCount() {
        //paso9
        if (lElementos!=null){
            return lElementos.size();
        }else {
            return 0;
        }
    }
}
